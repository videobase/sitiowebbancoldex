
    <section id="proyectos-slider" style="z-index: 12;">
        <div id="PrevTrailer" class="prev"><img src="<?php bloginfo('template_url'); ?>/img/arrow-left.png" alt="Arrow Slider" title="Arrow Slider"></div>
        <div id="NextTrailer" class="next"><img src="<?php bloginfo('template_url'); ?>/img/arrow-right.png" alt="Arrow Slider" title="Arrow Slider"></div>
        <div class="owl-carousel owl-theme sliderProyecto">
            <?php
            echo $catSlider;
            $defaults = array(
                    'posts_per_page' => 5000,
                    'category_name' => $catSlider,
                    'post_type' => 'slider-proyectos',
                );
            $posts = get_posts($defaults);
            if (have_posts()) {
                foreach ($posts as $post){
                    $videoSlider = rwmb_meta( 'video-slider');
                    $videoSliderFirefox = str_replace("watch?v=","embed/", $videoSlider);
                    $videoSliderId = str_replace("https://www.youtube.com/watch?v=","", $videoSlider);
                    $imagenesSlider = rwmb_meta( 'slider-proyectos' );
                    $imagenSlider = array_shift($imagenesSlider);
                    if ( !empty($videoSlider) ) {
                        if (isset($_SERVER['HTTP_USER_AGENT'])) {
                            $agent = $_SERVER['HTTP_USER_AGENT'];
                            if (strlen(strstr($agent, 'Firefox')) > 0) {
                                ?> 
                                <div class="item item-video">
                                    <div class="centrado-video">
                                        <div class="contVideo_youtube">
                                            <iframe class="videoYoutube" src="<?php echo $videoSliderFirefox; ?>"  frameborder="0" allowfullscreen ></iframe>
                                        </div>                                
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?> 
                                <div class="item item-video">
                                    <div class="centrado-video">
                                        <div class="contVideo_youtube">
                                            <div class="videoYoutube" idRef="<?php echo $videoSliderId; ?>" ></div>
                                        </div>                                
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    } else if ( !empty($imagenSlider) ){
                        ?>
                        <div class="item">
                            <img src="<?php echo $imagenSlider['full_url']; ?>" alt="Slider proyectos">
                        </div>
                        <?php
                    }
                }
            }
            wp_reset_query();
            ?>
        </div>
        </div>
    </section>