<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="Resource-type" content="Document" />
	<title><?php wp_title() ?></title>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/css/bootstrap.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/css/normlize.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/css/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/css/owlcarousel/owl.theme.default.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/css/owlcarousel/animate.css">	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
	<!-- METATAGS PARA POSICIONAR -->
	<meta name="author" content="Videobase Agencia" />
	<meta name="Title" content="Creciendo una nación">
<!-- 
	<meta name=”description” content="Creciendo una nación ... 140 caracteres ..." />
	<meta property="og:image" content="<?php bloginfo('template_url'); ?>/img/bancoldex-fb.png" />
	<meta property="og:title" content="Creciendo una nación ..." />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="Creciendo una nación ... 140 caracteres ...">
	<meta property="og:url" content="Creciendo una nación ... url ..." />
	 
 -->	<meta name="twitter:card" content="Landing Page" />
	<meta name="twitter:site" content="@..." />
	<meta name="twitter:title" content="Creciendo una nación ..." />
	<meta name="twitter:description" content="Creciendo una nación ... 140 caracteres ..." />
	<meta name="twitter:image" content="<?php bloginfo('template_url'); ?>/img/bancoldex-fb.png" />
</head>
<body>
	<main>
		<section id="header">
			<div class="titulo-landing">
				<h1><span>Creciendo</span> Una nación</h1>
			</div>
			<div class="logo-empresa">
				<a href="<?php echo home_url(); ?>">
					<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/logo-bancoldex.png" alt="">					
				</a>
			</div>
		</section>