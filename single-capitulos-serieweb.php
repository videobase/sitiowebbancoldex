<?php  

get_header();
global $post;
$post_slug=$post->post_name;
$tituloPrincipal = rwmb_meta('titulo-cap-serieWeb');
$textoCortoPrincipal = rwmb_meta('texto-corto-serieWeb');
$textoPrincipal = rwmb_meta( 'texto-cap-serieWeb');
$urlVideoPrincipal = rwmb_meta( 'url-capitulo');
$sliderInograficos = rwmb_meta('infograficos-capitulo');
$sliderpodCastInfograficos = rwmb_meta('url-soundCloud');
$categorias = get_categories();


?>

<div class="botonesScroll-capitulo">
	<div class="botonScroll">
		<a href="#informacion">
			<div class="btnTexto">Sinopsis</div>
			<div class="btnFranja"></div>
		</a>
	</div>
	<div class="botonScroll">
		<a href="#soundCloud">
			<div class="btnTexto">Audio</div>
			<div class="btnFranja"></div>
		</a>
	</div>
	<div class="botonScroll">
		<a href="#infografico">
			<div class="btnTexto">Infográfico</div>
			<div class="btnFranja"></div>
		</a>
	</div>
</div>
<section id="video-bancoldex">
	<div class="titulares-video">
		<div class="titulares-video-cont">
			<div class="cont-abs">
				<div class="dropdown temporadas">
				  <div class="span">
				  		<span relSulgActivo="<?php echo $categorias[0]->slug; ?>"><?php echo $categorias[0]->cat_name; ?></span>
						<div class="icono-bajar">
							<img src="<?php bloginfo('template_url'); ?>/img/drop-down.png" alt="">
						</div>
				  </div>
				  <div class="dropdown-content">
					<?php
						foreach( $categorias as $category ) {
						?>
							<div class="dropdown-item temporadas" relSlug="<?php  echo $category->slug;  ?>"><p><?php echo $category->name; ?></p></div>
						<?php  				  	
						}
					?>
				  </div>
				</div>
			  	<?php
		  		foreach( $categorias as $category ) {
			  	?>
				<div id="<?php echo $category->slug; ?>" class="dropdown capitulos">
				  <div class="span">
				  		<span><?php echo get_the_title(); ?></span>
						<div class="icono-bajar">
							<img src="<?php bloginfo('template_url'); ?>/img/drop-down.png" alt="">
						</div>
				  </div>
				  	<div class="dropdown-content">
					  	<?php
	                        $args = array(
	                            'post_type' => 'capitulos-serieweb',
	                            'category_name'  => $category->slug,
							    'order' => 'DESC'
	                        );
	                        $query = new WP_Query( $args );
	                        if ( $query->have_posts() ) : 
	                            while ( $query->have_posts() ) : $query->the_post();  
	                        ?>
	                            <div class="dropdown-item capitulos" urlRef="<?php the_permalink(); ?>" urlTrim="<?php echo $post_slug; ?>"><p><?php the_title(); ?></p></div>
	                        <?php 
	                            endwhile;
	                            wp_reset_postdata();
	                        else :
	                            esc_html_e('No hay capitulos');
	                        endif;
					  	?>
					</div>
				</div>
				<?php
                }
			  	?>
				<div class="titulo-capitulo">
					<h2><?php the_title(); ?></h2>
					<div class="snippet-capitulo">
						<?php echo $textoCortoPrincipal; ?>
					</div>
				</div>
				<div class="ver-capitulo">
					<p>
						Ver capitulo
					</p>
				</div>
			</div>
		</div>	
	</div>
	<div class="volver-seleccion-capt">
		<img src="<?php bloginfo('template_url'); ?>/img/entrar-menu.png" alt="">
	</div>
	<div class="slider-video">
		<?php  
			if ( !empty( $urlVideoPrincipal ) ) {
				$urlCap = str_replace("watch?v=","embed/", $urlVideoPrincipal);
			?>
				<iframe class="video" src="<?php echo $urlCap; ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<?php
			} else {
			?>
				<iframe class="video" src="https://www.youtube.com/embed/zj9BLnL5EiQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<?php
			}
			
		?>
	</div>
</section>
<section id="informacion" class="interior-seccion-capitulo">
	<div class="info-imagen interior-capitulo"></div>
	<div class="info-texto interior-capitulo">
		<h2>
			<?php echo $tituloPrincipal; ?>
		</h2>
		<?php
			echo $textoPrincipal;
		?>
	</div>
</section>
<section id="soundCloud">
	<div class="cont-soundCloud">
			<?php
				if ( !empty( $sliderpodCastInfograficos ) ) {
					foreach ($sliderpodCastInfograficos  as $podCast ) {
			?>
				<div class="soundCloud-item">
					<iframe width="100%" height="166" scrolling="no" frameborder="no" src="<?php echo $podCast; ?>"></iframe>
				</div>
			<?php
					}
				}else{
					?>
						<div class="soundCloud-item">
							<h2 style="color:#FFFFFF; text-align: center; padding: 2% 0%;">No hay Audios disponibles</h2>
						</div>							
					<?php
				}
			?>
	</div>
</section>
<section id="infografico">
	<div id="prev-infografico" class="prev"><img src="<?php bloginfo('template_url'); ?>/img/arrow-left.png" alt="Arrow Slider" title="Arrow Slider"></div>
	<div id="next-infografico" class="next"><img src="<?php bloginfo('template_url'); ?>/img/arrow-right.png" alt="Arrow Slider" title="Arrow Slider"></div>
	<div class="owl-carousel owl-theme slider-infografico">
			<?php
				if ( !empty( $sliderInograficos ) ) {
					foreach ($sliderInograficos  as $imagenSerieWeb ) {
			?>
					<div class="item infograficos">
						<div class="redes-contenedor">
							<div class="redes-cover">
								<div class="redes">
									<div class="red" rule="fb" st_username='Bancoldex' st_title='<?php echo get_the_title(); ?>' st_url='<?php the_permalink(); ?>' st_desc="<?php echo $textoCortoPrincipal; ?>" urlImage="<?php echo $imagenSerieWeb['full_url']; ?>">
										<img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/fb-share.png" alt="">
									</div>
									<div class="red" rule="tw" st_username='Bancoldex' st_title='<?php echo get_the_title(); ?>' st_url='<?php the_permalink(); ?>' st_desc="<?php echo $textoCortoPrincipal; ?>"  >
										<img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/tw-share.png" alt="">
									</div>
									<div class="red" rule="ln" st_username='Bancoldex' st_title='<?php echo get_the_title(); ?>' st_url='<?php the_permalink(); ?>' st_desc="<?php echo $textoCortoPrincipal; ?>"  >
										<img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/ln-share.png" alt="">
									</div>
								</div>
							</div>
						</div>
						<img class="img-responsive " src="<?php echo $imagenSerieWeb['full_url']; ?>"  />
					</div>
			<?php
					}
				}
			?>
	</div>
</section> 
<section id="footer">
	<div class="logos-patrocinadores">
		<div class="logo">
			<a href="http://www.mincit.gov.co/">
			<img src="<?php bloginfo('template_url'); ?>/img/logo-footer-industria.png" alt="">
			</a>
		</div>
		<div class="separador">
				<img src="<?php bloginfo('template_url'); ?>/img/separador.png" alt="">
		</div>
		<div class="logo">
			<a href="https://www.bancoldex.com/portal/default.aspx">
				<img src="<?php bloginfo('template_url'); ?>/img/logo-footer-bancoldex.png" alt="">
			</a>
		</div>
		<div class="separador">
			<img src="<?php bloginfo('template_url'); ?>/img/separador.png" alt="">
		</div>
		<div class="logo">
			<a href="https://www.dnp.gov.co/Paginas/inicio.aspx">
				<img src="<?php bloginfo('template_url'); ?>/img/logo-footer-nuevo-pais.png" alt="">
			</a>
		</div>
	</div>
	<div class="redes-sociales">
		<div class="cont-redes">
			<div class="siguenos">
				Síguenos en:
			</div>
			<div class="red">
				<a href="https://www.facebook.com/bancoldex/" target="blank"><img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/fb.png" alt=""></a>
			</div>
			<div class="red">
				<a href="https://twitter.com/Bancoldex" target="blank"><img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/tw.png" alt=""></a>
			</div>
			<div class="red">
				<a href="https://www.linkedin.com/company-beta/73030/" target="blank"><img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/ln.png" alt=""></a>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();

?>		