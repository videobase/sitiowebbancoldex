<?php  
/* 
 * Template Name: Plantilla Inicio
 * 
 * Plantilla para el Inicio
 * 
 * 2.09.2017
 */

get_header();

$tituloPrincipal = rwmb_meta( 'titulo-serieWeb');
$textoPrincipal = rwmb_meta( 'texto-serieWeb');

?>
<div class="MSchemGoo">
    <h2>Creciendo una nación -  Serie de Bancoldex</h2>
    <div itemscope="" itemtype="http://schema.org/Corporation">
    <a title="Bancoldex Serie Web" itemprop="url" href="http://www.creciendounanacion.com/"><div itemprop="name"><strong>Bancoldex</strong></div>
    </a>
    <div itemprop="description">
     En Colombia hay empresarios de diferentes regiones del país. Bancoldex te invita a conocer las historias que hacen grande una nación, visita nuestra serie web
    </div>
    <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
    <span itemprop="streetAddress">Cl. 7 Sur #4270, Medellín, Antioquia</span><br>
    <span itemprop="addressLocality">Medellin</span><br>
    <span itemprop="addressRegion">Antioquia</span><br>
    <span itemprop="addressCountry">Colombia</span><br>
    </div>
    </div>
</div>

<section id="video-bancoldex">
<!-- 
	<div id="prev-inicio" class="prev"><img src="<?php //bloginfo('template_url'); ?>/img/arrow-left.png" alt="Arrow Slider" title="Arrow Slider"></div>
	<div id="next-inicio" class="next"><img src="<?php //bloginfo('template_url'); ?>/img/arrow-right.png" alt="Arrow Slider" title="Arrow Slider"></div>
 -->	
 	<div class="owl-carousel owl-theme slider-inicio">
		<?php
		// CAPITULOS RECOMENDADOS
        $argsSlider = array( 'post_type' => 'capitulos-serieweb' , 'order'  => 'DESC' );
        $loop = new WP_Query( $argsSlider );
            while ( $loop->have_posts() ) : $loop->the_post();
            	$checkRecomendado = rwmb_meta('capitulos-recomendados', $args, $post->ID);
                $youtube = rwmb_meta('url-trailer-capitulo', $args, $post->ID);
                $youtubeCtrl = $youtube.'?version=3&enablejsapi=1';
                $img_url = wp_get_attachment_image_src(get_post_thumbnail_id());
                if ($checkRecomendado == 1) {
	             	if ($youtube) {
	             	?>
						<div class="item">
							<div class="titulares-video">
								<div class="titulares-video-cont">
									<div class="cont-abs sliderPpal">
										<div class="ver-capitulo">
											<a href="#">
												Ir a capitulo
											</a>
										</div>
										<div class="ver-capitulo">
											<p>
												Ver Trailer
											</p>
										</div>

									</div>
								</div>	
							</div>
							<div class="volver-seleccion-capt">
								<img src="img/entrar-menu.png" alt="">
							</div>
							<iframe class="video" src="<?php echo str_replace("watch?v=","embed/", $youtubeCtrl ); ?>" allowfullscreen></iframe>
						</div>					
					<?php
	             	}else{
	        		?>
						<div class="item">
							<a href="<?php the_permalink(); ?>">
								<?php
								the_post_thumbnail( 'full' , array( 'class' => 'img-responsive' ) , array( 'title' => get_the_title() ) , array( 'alt' => get_the_title() ) );  
								?>
							</a>			
						</div>					
	                <?php
	             	}
                }
            endwhile;

    wp_reset_postdata();

	?>
		<?php
		// CAPITULOS RECOMENDADOS
        $argsSlider = array( 'post_type' => 'slider-home' , 'order'  => 'DESC' );
        $loop = new WP_Query( $argsSlider );
            while ( $loop->have_posts() ) : $loop->the_post();
            	$checkRecomendado = rwmb_meta('capitulos-recomendados', $args, $post->ID);
            	$urlSlide = rwmb_meta('url-imagen-destcada', $args, $post->ID);
                $youtube = rwmb_meta('url-trailer-capitulo-unplug', $args, $post->ID);
                $youtubeCtrl = $youtube.'?version=3&enablejsapi=1';
                $img_url = wp_get_attachment_image_src(get_post_thumbnail_id());
             	if ($youtube) {
             	?>
					<div class="item videoWrap">
						<iframe class="video" src="<?php echo str_replace("watch?v=","embed/", $youtubeCtrl ); ?>" allowfullscreen></iframe>
					</div>					
				<?php
             	}else{
        		?>
					<div class="item">
						<a href="<?php echo $urlSlide; ?>">
							<?php
							the_post_thumbnail( 'full' , array( 'class' => 'img-responsive slider-img' ) , array( 'title' => get_the_title() ) , array( 'alt' => get_the_title() ) );  
							?>
						</a>
					</div>					
                <?php
             	}

            endwhile;
        ?>
	</div>
	<?php  
            wp_reset_postdata();
	?>

</section>
<section id="informacion">
	<div class="info-texto">
		<h2><?php echo $tituloPrincipal; ?></h2>
		<?php echo $textoPrincipal; ?>
	</div>
	<div class="info-imagen">
		<img src="<?php bloginfo('template_url'); ?>/img/imagen-colombia.png" alt="">
	</div>
</section>
<section id="footer">
	<div class="logos-patrocinadores">
		<div class="logo">
			<a href="http://www.mincit.gov.co/">
			<img src="<?php bloginfo('template_url'); ?>/img/logo-footer-industria.png" alt="">
			</a>
		</div>
		<div class="separador">
				<img src="<?php bloginfo('template_url'); ?>/img/separador.png" alt="">
		</div>
		<div class="logo">
			<a href="https://www.bancoldex.com/portal/default.aspx">
				<img src="<?php bloginfo('template_url'); ?>/img/logo-footer-bancoldex.png" alt="">
			</a>
		</div>
		<div class="separador">
			<img src="<?php bloginfo('template_url'); ?>/img/separador.png" alt="">
		</div>
		<div class="logo">
			<a href="https://www.dnp.gov.co/Paginas/inicio.aspx">
				<img src="<?php bloginfo('template_url'); ?>/img/logo-footer-nuevo-pais.png" alt="">
			</a>
		</div>
	</div>
	<div class="redes-sociales">
		<div class="cont-redes">
			<div class="siguenos">
				Síguenos en:
			</div>
			<div class="red">
				<a href="https://www.facebook.com/bancoldex/" target="blank"><img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/fb.png" alt=""></a>
			</div>
			<div class="red">
				<a href="https://twitter.com/Bancoldex" target="blank"><img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/tw.png" alt=""></a>
			</div>
			<div class="red">
				<a href="https://www.linkedin.com/company-beta/73030/" target="blank"><img src="<?php bloginfo('template_url'); ?>/img/redes-sociales/ln.png" alt=""></a>
			</div>
		</div>
	</div>
</section>
<?php 
get_footer();
?>