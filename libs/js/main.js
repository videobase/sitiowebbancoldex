$(document).ready(function(){
    // ACTIVACIONES DE SLIDERS CONTROLANDO QUE EXISTE CONTENEDOR
    if ($('.slider-inicio').length !== 0) {
        sliderOwl('slider-inicio' , '' , '' , 'video-bancoldex');        
    }
    if ($('.slider-infografico').length !== 0) {
        sliderOwl('slider-infografico' , 'prev-infografico' , 'next-infografico' , 'infografico');
    }
    resizeVideo();
    animacionesInterfaces();    
	//FUNCIONES ONRESIZE
	$( window ).resize(function() {
		resizeVideo();
	});	
});

function sliderOwl(clase , btnPrev , btnNext , padre){

    var sliderFull = $('.'+clase);
    // CONFIGURAR EL AUTOWITH
    sliderFull.find('.item').each(function(){
        $(this).css({'width' : $('section#'+padre ).width() });
    });
    if (clase === 'slider-inicio') {
        sliderFull.owlCarousel({
            loop: true,
            autoWidth:true,
            animateOut: 'fadeOutUp',
            animateIn: 'fadeInDown',
            items:1,
            nav:false,
            dots: true,
            autoplay: true,
            smartSpeed:850,
            rewindSpeed: 800,
        })
    }else{
        // INICIALIZA SLIDER
        sliderFull.owlCarousel({
            autoWidth:true,
            animateOut: 'fadeOutUp',
            animateIn: 'fadeInDown',
            items:1,
            loop: true,
            autoplay: false,
            dots: false,
            video:true,
            smartSpeed:850,
            rewindSpeed: 500,
        });        
    }

    if (btnPrev) {
        $('#'+btnPrev).on('click' , function(){
            sliderFull.trigger('prev.owl.carousel');            
        });
    }
    if (btnNext) {
        $('#'+btnNext).on('click' , function(){
            sliderFull.trigger('next.owl.carousel');            
        });
    }
}
function resizeVideo(){
	// $('section#video-bancoldex , section#video-bancoldex .slider-inicio .item').css({
	// 	'height' : $(window).width() * 0.562
	// });
}
function animacionesInterfaces(){
    var temporadaActual = $('.dropdown.temporadas .span').find('span').attr('relsulgactivo');
    if ($('#'+temporadaActual).length !== 0) {
        $('#'+temporadaActual).toggleClass('activo');
    }


    var animaDrop = new TimelineMax({ paused: true , ease:Linear.easeNone });
    animaDrop.staggerTo($('.dropdown-item') , 0.4 , { opacity : 1} , 0.2);
    
    $('body').on('click', '.dropdown.temporadas .span', function() {
        $(this).parent().find('.dropdown-content').toggleClass('activo');
        if ($(this).parent().parent().find('.dropdown.capitulos.activo').find('.dropdown-content.activo').length !== 0) {
            $(this).parent().parent().find('.dropdown.capitulos.activo').find('.dropdown-content.activo').removeClass('activo');
        }
        animaDrop.play();
    });

    $('body').on('click', '.dropdown.capitulos.activo .span', function() {
        $(this).parent().find('.dropdown-content').toggleClass('activo');
        if ($(this).parent().parent().find('.dropdown.temporadas').find('.dropdown-content.activo').length !== 0) {
            $(this).parent().parent().find('.dropdown.temporadas').find('.dropdown-content.activo').removeClass('activo');
        }
        animaDrop.play();
    });

    var animaCortinaCap =  new TimelineMax({ paused: true , ease:Linear.easeNone });
    animaCortinaCap.to($('.cont-abs') , 0.2 , { left: '30%', opacity : 0 , autoAlpha: 1, display: 'none'})
    animaCortinaCap.to($('.volver-seleccion-capt') , 0.2 , { opacity : 1 , autoAlpha: 1, display: 'block'} , 0)
    animaCortinaCap.to($('.titulares-video') , 0.4 , { autoAlpha: 1, display: 'none'})

    $('body').on('click', '.ver-capitulo', function() {
        animaCortinaCap.play();
    });
    $('body').on('click', '.volver-seleccion-capt', function() {
        animaCortinaCap.reverse();
    });
    $('.dropdown-item.temporadas').each(function() {
        var temporada = $(this).find('p').html();
        var temporadaSlug =  $(this).attr('relSlug');
        $(this).on('click', function(event) {
            $('.dropdown.temporadas .span span').html(temporada);
            $(this).parent().parent().parent().find('.dropdown.capitulos').find('.span').find('span').text('Elegir Capitulo');
            $(this).parent().parent().parent().find('.dropdown.capitulos.activo').find('.span').find('span').text('Elegir Capitulo');            
            $(this).parent().toggleClass('activo');
            $('.dropdown.capitulos').removeClass('activo');
            $('#'+temporadaSlug).addClass('activo');
        });
    });
    $('.dropdown-item.capitulos').each(function() {
        var capitulo = $(this).find('p').html();
        var capituloSlug =  $(this).attr('relSlug');
        var urlActual = $(this).attr('urlRef');
        $(this).on('click', function() {
            $('.dropdown.capitulos .span span').html(capitulo);
            $(this).parent().toggleClass('activo');

            verCapitulo(urlActual);
            $('.dropdown-item').css({'opacity' : 0})
            animaDrop.play();
        });
    });
    $('.item.infograficos').each(function() {
        var animaRedesCont =  new TimelineMax({ paused: true , ease:Linear.easeNone });
        animaRedesCont.to($(this).find('.redes-contenedor') , 0.2 , { opacity: 1 ,  force3D:false})
        $(this).mouseenter(function() {
            animaRedesCont.play();     
        }).mouseleave(function() {
            animaRedesCont.reverse();
        });
    });
    animaRedes();
    compartirInfograficos();
    $('section#header').on('click', function() {
        var url = "http://www.creciendounanacion.com/";    
        $(location).attr('href',url);
    });
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });

}
function verCapitulo(url){
    $(location).attr('href',url);
    animaDrop.reverse();
}
function animaRedes(){
    $('.red').each(function() {
        var animaRed =  new TimelineMax({ paused: true , ease:Linear.easeNone });
        animaRed.to($(this) , 0.2 , { scale: 1.2, opacity: 1 ,  force3D:false})
        $(this).mouseenter(function() {
            animaRed.play();
        }).mouseleave(function() {
            animaRed.reverse();
        });
    });
}
function compartirInfograficos(){

    $('.red').each(function(){
            var rule = $(this).attr('rule');
            var urlImage = $(this).attr('urlImage');
            var url = $(this).attr('st_url');
            var descp = $(this).attr('st_desc');            
            var title = $(this).attr('st_title');
            $(this).on('click' , function(){
                if (rule == 'fb') {
                    // sharefbimage(title , url , urlImage , descp.replace(/(<([^>]+)>)/ig,"") )
                    var facebook = 'http://www.facebook.com/sharer.php?u='+url+'&amp;t='+title+'&picture='+urlImage;
                    window.open(facebook , 'facebook-share-dialog', 'width=450,height=450');
                    // window.open('https://www.facebook.com/sharer/sharer.php?app_id=324809347996102&u='+url+'&picture='+urlImage+'', 'facebook-share-dialog', 'width=450,height=450');
                }
                if (rule == 'tw') {
                        window.open('http://twitter.com/home?status='+title+' - '+url+' - vía @Bancoldex','Compartir en Twitter', config='height=300, width=500');
                }
                if (rule == 'ln') {
                    // 'http://www.linkedin.com/shareArticle?url='+url+'&amp;title='+title
                        window.open('https://www.linkedin.com/shareArticle?mini=true&url='+url+'&title='+title+'&source=vía @Bancoldex');
                }
            })
    })    
}

function sharefbimage(title , url , urlImage , descp) {
    FB.init({ appId:'324809347996102', status: true, cookie: true , version: 'v2.11'});
    FB.ui({
        method: 'share',
        action_type: 'og.shares',
        action_properties: JSON.stringify({
            object : {
               'og:url': url,
               'og:title': title,
               'og:description': descp,
               'og:og:image:width': '2560',
               'og:image:height': '960',
               'og:image': urlImage
            }
        })
    });
}