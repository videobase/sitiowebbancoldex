<?php  

if ( function_exists( 'add_theme_support' ) )
    add_theme_support( 'post-thumbnails' );
    	

add_filter( 'rwmb_meta_boxes', 'box_personalizados' );
function box_personalizados( $meta_boxes ) {
	// INICIO SERIE WEB	
	$meta_boxes[] = array(
		'title'      => __( '1. Contenido Serie web', 'textdomain' ),
		'post_types' => 'page',
		'fields'     => array(
			array(
				'id'   => 'titulo-serieWeb',
				'name' => __( 'Titulo Principal Serie Web', 'textdomain' ),
				'type'    => 'text'
			),
            array(
                'type' => 'divider',
            ),
			array(
				'id'   => 'texto-serieWeb',
				'name' => __( 'Texto Serie Web', 'textdomain' ),
                'rows' => 5,
                'type'    => 'wysiwyg',
                'std'     => esc_html__( '' ),
			),
		),
	);
	// CAPITULO SERIE WEB
	$meta_boxes[] = array(
		'title'      => __( '1. Contenido Capitulo de la Serie web', 'textdomain' ),
		'post_types' => 'capitulos-serieweb',
		'fields'     => array(
			array(
				'id'   => 'titulo-cap-serieWeb',
				'name' => __( 'Titulo del Capítulo Serie Web', 'textdomain' ),
				'type'    => 'text'
			),
            array(
                'type' => 'divider',
            ),
			array(
				'id'   => 'texto-corto-serieWeb',
				'name' => __( 'Texto Corto del Capítulo', 'textdomain' ),
                'rows' => 5,
                'type'    => 'wysiwyg',
                'std'     => esc_html__( '' ),
			),
            array(
                'type' => 'divider',
            ),
			array(
				'id'   => 'texto-cap-serieWeb',
				'name' => __( 'Texto del Capítulo Serie Web', 'textdomain' ),
                'rows' => 5,
                'type'    => 'wysiwyg',
                'std'     => esc_html__( '' ),
			),
			array(
				'id'   => 'url-capitulo',
				'name' => __( 'video del Capítulo', 'textdomain' ),
				'label_description' => esc_html__( 'url de youtube' ),
				'type'    => 'text'
			),
			array(
				'id'   => 'url-trailer-capitulo',
				'name' => __( 'trailer del Capítulo', 'textdomain' ),
				'label_description' => esc_html__( 'url de youtube' ),
				'type'    => 'text'
			),
		),
	);
	// LISTAS DE REPRODUCCIÓN SOUNDCLOUD
	$meta_boxes[] = array(
		'title'      => __( '2. Listas de podCast SoundCloud', 'textdomain' ),
		'post_types' => 'capitulos-serieweb',
		'fields'     => array(
			array(
				'id'   => 'url-soundCloud',
				'name' => __( 'Crear podCast de SoundCloud', 'textdomain' ),
				'type'  => 'text',
				'clone' => true,
			),
		),
	);

	// SLIDER INFOGRAFICOS CAPIUTLO
	$meta_boxes[] = array(
		'title'      => __( '3. Slider Infográficos del capítulo', 'textdomain' ),
		'post_types' => 'capitulos-serieweb',
		'fields'     => array(
			// THICKBOX IMAGE UPLOAD (WP 3.3+)
			array(
				'name' => __( 'Para optimizar el slider es importante que todas las imagenes tengan una medida similar(Recomendamos 1920X1080)', 'textdomain' ),
				'label_description' => esc_html__( 'Peso Máximo 150MB por imagen.' ),
				'id'               => 'infograficos-capitulo',
				'type'             => 'plupload_image',
				'force_delete'     => false,
			),
		),
	);

	// AGREGA A SLIDER PRINCIPAL
	$meta_boxes[] = array(
		'title'      => __( '4. Agregar capitulo a slider de inicio', 'textdomain' ),
		'post_types' => 'capitulos-serieweb',
		'fields'     => array(
			array(
				'name' => esc_html__( 'Agregar a slider de inicio', 'textdomain' ),
				'id'   => 'capitulos-recomendados',
				'type' => 'checkbox',
				'std'  => 0,
			),
		),
	);

	// CAPITULO SERIE WEB
	$meta_boxes[] = array(
		'title'      => __( '1. Enlaces slider', 'textdomain' ),
		'post_types' => 'slider-home',
		'fields'     => array(
			array(
				'id'   => 'url-imagen-destcada',
				'name' => __( 'Link de redirección de slide', 'textdomain' ),
				'type'    => 'text'
			),
			array(
				'id'   => 'url-trailer-capitulo-unplug',
				'name' => __( 'Trailer del Capítulo', 'textdomain' ),
				'label_description' => esc_html__( 'url de youtube' ),
				'type'    => 'text'
			),
		),
	);

	return $meta_boxes;
}

add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

// * Hide editor on specific pages.

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  // Hide the editor on the page titled 'Homepage'
  $homepgname = get_the_title($post_id);
  if($homepgname == 'Creciendo una Nación'){ 
    remove_post_type_support('page', 'editor');
  }
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'my-page-template.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}

?>